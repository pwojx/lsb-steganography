sfml-app: main.o
	g++ main.o -o sfml-app -lsfml-graphics

main.o: main.cpp
	g++ -c main.cpp

edit:
	vim main.cpp

clean:
	rm -f main.o sfml-app
