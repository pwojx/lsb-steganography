#include <iostream>
#include <SFML/Graphics.hpp>

int hide_message(const std::string& msg, const std::string& if_name, const std::string& of_name);
std::string recover_message(const std::string& f_name, const unsigned int msg_len);

int hide_image(const std::string& si_name, const std::string& if_name, const std::string& of_name);
sf::Image recover_image(const std::string& f_name, const sf::Vector2u si_size);

int main(int argc, char *argv[]) {
	// A text message to be hidden inside an image.
	std::string msg = "5t3g4n0gr4phy 15 4w3s0m3!";

	// Hiding the message.
	hide_message(msg, "lena.png", "msg_result.png");

	// Recovering the message.	
	std::string rec_msg = recover_message("msg_result.png", 25);
	std::cout << "Recovered message: " << rec_msg << std::endl;

	// Hiding the secret image.
	hide_image("names.png", "lena.png", "im_result.png");
	
	// Recovering the secret image.
	const sf::Vector2u si_size(512, 512);
	sf::Image rec_im = recover_image("im_result.png", si_size);
	rec_im.saveToFile("recovered_im.png");
	std::cout << "Recovered image has been saved." << std::endl;

	// Exit.
	std::cout << "OK" << std::endl;
	return 0;
}

int hide_message(const std::string& msg, const std::string& if_name, const std::string& of_name) {
	// The image in which the text message will be hidden.
	sf::Image im;
	if (!im.loadFromFile(if_name)) {
		std::cerr << "Could not load the specified image" << std::endl;
		return -1;
	}

	// The size of the image in pixels.
	unsigned int im_size = im.getSize().x * im.getSize().y;

	// Make sure there is enough space in the image file to store the message.
	if (im_size < msg.length() * 8) {
		std::cerr << "The image size is too small to hide the message" << std::endl;
		return -1;
	}

	// Prepare a new image based on the given one.
	sf::Image res;
	res.create(im.getSize().x, im.getSize().y, sf::Color::Black);
	res.copy(im, 0, 0);

	// Hide the message inside the image.
	for (int i = 0; i < msg.length() * 8; ++i) {
		sf::Color pixel = res.getPixel(i % im.getSize().x, i / im.getSize().x);

		bool bit = msg[i / 8] & (1 << (i % 8));
		if (bit == false) {
			pixel.r &= ~(1 << 0);
			pixel.b &= ~(1 << 0);
			pixel.g &= ~(1 << 0);
		} else {
			pixel.r |= (1 << 0);
			pixel.b |= (1 << 0);
			pixel.g |= (1 << 0);
		}

		res.setPixel(i % im.getSize().x, i / im.getSize().x, pixel);
	}

	// Save the result in a file.
	if (!res.saveToFile(of_name)) {
		std::cerr << "The file could not be saved" << std::endl;
		return -1;
	}

	return 0;
}

std::string recover_message(const std::string& f_name, const unsigned int msg_len) {
	// A recovered message. 
	std::string hidden_msg;

	// An image with a hidden text message.
	sf::Image im;
	if (!im.loadFromFile(f_name)) {
		std::cerr << "Could not load the specified image" << std::endl;
		return {};
	}

	// Recover the message from the image.
	char c;
	for (int i = 0; i < msg_len * 8; ++i) {
		if (i % 8 == 0)
			c = '\0';

		sf::Color pixel = im.getPixel(i % im.getSize().x, i / im.getSize().x);

		bool bit = pixel.r & (1 << 0);
		if (bit == true)
			c |= (1 << (i % 8));	

		if (i % 8 == 7)
			hidden_msg += c;
	}

	return hidden_msg;
}

int hide_image(const std::string& si_name, const std::string& if_name, const std::string& of_name) {
	// An image to be hidden.
	sf::Image secret;
	if (!secret.loadFromFile(si_name)) {
		std::cerr << "Could not load the specified image" << std::endl;
		return -1;
	}
	
	// An image in which the secret image will be hidden.
	sf::Image im;
	if (!im.loadFromFile(if_name)) {
		std::cerr << "Could not load the specified image" << std::endl;
		return -1;
	}

	// The size of the secret image in pixels.
	unsigned int secret_size = im.getSize().x * im.getSize().y;

	// The size of the image in pixels.
	unsigned int im_size = im.getSize().x * im.getSize().y;

	// Make sure there is enough space in the image file to store the secret image.
	if (im_size < secret_size) {
		std::cerr << "The image size is too small to hide the secret image" << std::endl;
		return -1;
	}

	// Prepare a new image based on the given one.
	sf::Image res;
	res.create(im.getSize().x, im.getSize().y, sf::Color::Black);
	res.copy(im, 0, 0);

	// Hide the secret image inside the image.
	for (int i = 0; i < secret_size; ++i) {
		sf::Color pixel = res.getPixel(i % res.getSize().x, i / res.getSize().x);
		sf::Color secret_pixel = secret.getPixel(i % secret.getSize().x, i / secret.getSize().x);
		
		if (secret_pixel.r > 0 || secret_pixel.g > 0 || secret_pixel.b > 0) {
			pixel.r |= (1 << 0);
			pixel.g |= (1 << 0);
			pixel.b |= (1 << 0);
		} else {
			pixel.r &= ~(1 << 0);
			pixel.g &= ~(1 << 0);
			pixel.b &= ~(1 << 0);
		}

		res.setPixel(i % res.getSize().x, i / res.getSize().x, pixel);
	}

	// Save the result in a file.
	if (!res.saveToFile(of_name)) {
		std::cerr << "The file could not be saved" << std::endl;
		return -1;
	}

	return 0;
}

sf::Image recover_image(const std::string& f_name, const sf::Vector2u si_size) {
	// A recovered image. 
	sf::Image hidden_im;
	hidden_im.create(si_size.x, si_size.y, sf::Color::Black);

	// An image with a hidden secret image.
	sf::Image im;
	if (!im.loadFromFile(f_name)) {
		std::cerr << "Could not load the specified image" << std::endl;
		return {};
	}

	// Recover the secret image from the image.
	for (int i = 0; i < si_size.x * si_size.y; ++i) {
		sf::Color pixel = im.getPixel(i % im.getSize().x, i / im.getSize().x);

		bool bit = pixel.r & (1 << 0);
		if (bit == true) {
			pixel.r = 255;
			pixel.g = 255;
			pixel.b = 255;
			pixel.a = 255;
			hidden_im.setPixel(i % si_size.x, i / si_size.x, pixel);
		}
	}

	return hidden_im;
}
